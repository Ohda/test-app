COMPOSE=cd ./ && docker-compose
CONSOLE=php bin/console

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

install: docker-build
#		$(COMPOSE) exec php-fpm composer install
#		$(COMPOSE) exec node yarn install

docker-build: ## Rebuild docker environment
	$(COMPOSE) build --no-cache
	$(COMPOSE) up -d --build --force-recreate

docker-up: ## Start or restart all the docker services
	$(COMPOSE) up -d

docker-down:  ## Stop and remove all the docker containers
	$(COMPOSE) down

docker-start: ## Start existing docker environment
	$(COMPOSE) start

docker-stop: ## Stop existing docker environment
	$(COMPOSE) stop

docker-log: ## Containers logs
	$(COMPOSE) logs

php: ## exec a shell php
	$(COMPOSE) exec php-fpm bash

node: ## Launch a shell into the container nodejs
	$(COMPOSE) exec node bash

## Remove not rules message
%:
	@:
